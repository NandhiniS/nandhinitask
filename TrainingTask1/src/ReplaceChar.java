import java.util.*;
import java.lang.*;
public class ReplaceChar {
public static void main(String args[])
{
	Scanner sn=new Scanner(System.in);

	String result="";
	char ch;
	
	System.out.println("Enter the String:"); 
	String s=sn.nextLine();
	
	
	System.out.println("Enter the Character You want to replace");
	

     ch = sn.next().charAt(0);
	result=replaceString(s,ch);
    
	System.out.println(result);
	
		
}
public static String replaceString(String  s,char ch)
{
	String result="";
	for(int i=0;i<s.length();i++)
	{
		char ch1=s.charAt(i);
		if (ch1 == ch) {
            result = result + 'b';
        } else {
            result = result + ch1;
        }
    }
	return result;
}
}